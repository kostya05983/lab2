﻿using System.Collections.Generic;

namespace Lab2
{
    public interface IMatrix
    {
        Matrix Add(Matrix matrix);
        Matrix Subtract(Matrix matrix);

        Matrix SwapRows(int rowIndex1, int rowIndex2);

        Matrix MultiplyRow(int rowIndex, double number);
        Matrix AddRow(int rowIndex1, int rowIndex2);

        Matrix Concat(Matrix matrix);
        Matrix Mirror();

        double GetElement(int rowIndex, int columnIndex);
        List<double> GetRow(int rowIndex);
        List<double> GetColumn(int columnIndex);

        int RawCount
        {
            get;
        }
        int ColumnCount
        {
            get;
        }

        double MNorm();
        double LNorm();
    }
}
