﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Gauss
    {
        private Matrix matrix_left,
                        matrix_right,
                        matrix_processing;

        public Gauss(Matrix matrix_left, Matrix matrix_right)
        {
            this.matrix_left = matrix_left;
            this.matrix_right = matrix_right;
        }

        public List<double> UnitCalculation()
        {
            StraightRun();
            BackStroke();
            return this.matrix_processing.GetColumn(this.matrix_processing.ColumnCount - 1);
        }

        private void StraightRun()
        {
            if (IsSquare())
            {
                if (!(FindNoZeroElement()[0] == 0))
                {
                    this.matrix_left.SwapRows(0, FindNoZeroElement()[0]);
                }

                this.matrix_left.Concat(this.matrix_right);
                this.matrix_processing = this.matrix_left;

                for (int i = 0; i < this.matrix_processing.RawCount; i++)
                {
                    FlipedProcess(i, 0);
                }
            }
            else throw new Exception("Ошибка! Матрица должна быть квадратной!");
        }

        private void BackStroke()
        {
            for (int i = this.matrix_processing.RawCount - 1; i > 0; i--)
            {
                FlipedProcess(i, 1);
            }
        }

        private void FlipedProcess(int startIndex, int type)
        {
            for (int k = startIndex + 1, i = startIndex - 1; (type == 0 ? k < this.matrix_processing.RawCount : i > 0); k++, i--)
            {
                multiply(startIndex, (type == 0 ? k : i));
            }
        }

        private void multiply(int startIndex, int currIndex)
        {
            double coefNorm = 1 / this.matrix_processing.GetElement(startIndex, startIndex),
            coeffK = -this.matrix_processing.GetElement(startIndex, currIndex),
            uncoeffk = -1.0 / this.matrix_processing.GetElement(startIndex, currIndex);

            this.matrix_processing.MultiplyRow(startIndex, coefNorm);
            this.matrix_processing.MultiplyRow(startIndex, coeffK);
            this.matrix_processing.AddRow(currIndex, startIndex);

            this.matrix_processing.MultiplyRow(startIndex, uncoeffk);
        }

        private int[] FindNoZeroElement()
        {
            for (int i = 0; i < this.matrix_left.ColumnCount; i++)
            {
                if (this.matrix_left.GetElement(i, 0) != 0)
                    return new int[] { i, 0 };
            }
            throw new Exception("ZERO!!!!");
        }

        private bool IsSquare() => this.matrix_left.RawCount == this.matrix_left.ColumnCount;
    }
}
