﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        static void Main(string[] args)
        {
           startGauss();   
           startIteration();
        }

        private static void startGauss()
        {
            Console.WriteLine("Метод Гаусса");
            Gauss nethod_gauss = new Gauss(new Matrix(new double[][] { new double[] { 15, 4, 1 }, new double[] { 0, 15, 1 }, new double[] { 1, 1, 15 } }), new Matrix(new double[] { 16, 20, 17 }, Matrix.MatrixType.Column));

            List<double> result = nethod_gauss.UnitCalculation();

            for (var i = 0; i < result.Count; i++)
                Console.WriteLine("Корень x" + (i+1) + "=" + result[i]);
        }

        private static void startIteration()
        {
            Console.WriteLine("Метод итераций");
            double[] beta = new double[] { 16,20,17};
            double[][] alpha =
            {
                new double[] {15,0,1 },
                new double[] {4,15,1},
                new double[] {1,1,15}
            };

            IMatrix[] inputMatrix = new IMatrix[2];
            inputMatrix[0] = new Matrix(alpha);
            inputMatrix[1] = new Matrix(beta, Matrix.MatrixType.Column);
            IterationMethod iterationMethod = new IterationMethod(inputMatrix,0.1);

            double[] buf= iterationMethod.MakeIteration();
            int iteration = iterationMethod.getIterations();

            Console.WriteLine("Точность 0.1");
            writeResult(buf,iteration);

            Console.WriteLine("Точность 0.01");
            iterationMethod.setAccuracy(0.01);
            buf=iterationMethod.MakeIteration();
            iteration = iterationMethod.getIterations();
            writeResult(buf, iteration);

            Console.WriteLine("Точность 0.001");
            iterationMethod.setAccuracy(0.001);
            buf=iterationMethod.MakeIteration();
            iteration = iterationMethod.getIterations();
            writeResult(buf, iteration);

        }

        private static void writeResult(double[] result,int iteration)
        {
            for(int i=0;i<result.Length;i++)
                Console.WriteLine("Корень x" + (i + 1) + "=" + result[i]);

            Console.WriteLine("Колличество итераций:" +iteration);
        }
    }
}
