﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class IterationMethod
    {
        private int iterations=0;
        private int n;
        private double accuracy;
        private IMatrix[] inputMatrix;
        private double[][] alpha;
        private double[] beta;

        public IterationMethod(IMatrix[] inputMatrix,double accuracy)
        {
            this.inputMatrix = inputMatrix;
            this.accuracy = accuracy;
        }

        public double[] MakeIteration()
        {
            n = inputMatrix[1].RawCount;
            for (var i = 0; i < inputMatrix[0].RawCount; i++)
                if (inputMatrix[0].GetElement(i, i) == 0)
                    throw new DivideByZeroException();

            Initialize();

           
            var norm = new Matrix(alpha).LNorm();

            if (norm >= 1)
                throw new ArgumentException();


            double exit = getExit(norm, accuracy);

            return getResult(exit);
        }

        public void setAccuracy(double accuracy)
        {
            this.accuracy = accuracy;
        }

        private void Initialize()
        {
             alpha = new double[n][];
            for (var i = 0; i < n; i++)
                alpha[i] = new double[n];

             beta = new double[n];
            for (var i = 0; i < n; i++)
            {
                beta[i] = inputMatrix[1].GetElement(i, 0) / inputMatrix[0].GetElement(i, i);
                for (var j = 0; j < n; j++)
                    if (i != j)
                        alpha[i][j] = -inputMatrix[0].GetElement(i, j) / inputMatrix[0].GetElement(i, i);
                    else
                        alpha[i][j] = 0;
            }
        }

        private double[] getResult(double exit)
        {
            var matrix = new Matrix[2];
            var x = new double[n];
            Array.Copy(beta, x, n);
            var bufx = new double[n];
            var k = 0;


            while (true)
            {
                Array.Copy(x, bufx, n);
                for (var i = 0; i < n; i++)
                {
                    x[i] = beta[i];
                    for (var j = 0; j < n; j++)
                        x[i] = x[i] + alpha[i][j] * bufx[j];
                }

                matrix[0] = new Matrix(bufx, Matrix.MatrixType.Column);
                matrix[1] = new Matrix(x, Matrix.MatrixType.Column);

                if (matrix[1].Subtract(matrix[0]).LNorm() <= exit)
                {
                    iterations = k;
                    return x;
                }

                k++;
            }
        }
       
        private double getExit(double norm,double accuracy)
        {
            if (norm < 0.5)
                return accuracy;
            else
                return (1 - norm) / norm * accuracy;
        }

        public int getIterations()
        {
            return iterations;
        }
    }
}
