﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab2
{
    public class Matrix : IMatrix
    {
        private List<List<double>> matrix;

        public enum MatrixType { Row, Column };

        public Matrix(double[] coefficients, MatrixType matrixType)
        {
            this.matrix = new List<List<double>>();
            switch (matrixType)
            {
                case MatrixType.Column:
                    foreach (var element in coefficients)
                    {
                        this.matrix.Add(new List<double> { element });
                    }
                    break;
                case MatrixType.Row:
                    this.matrix.Add(coefficients.ToList());
                    break;
                default:
                    throw new Exception();
            }
        }
        public Matrix(List<double> coefficients, MatrixType matrixType) : this(coefficients.ToArray(), matrixType) { }
        public Matrix(double[][] coefficients)
        {
            this.matrix = new List<List<double>>();
            for (int i = 0; i < coefficients.Length; i++)
            {
                this.matrix.Add(coefficients[i].ToList());
            }
        }
        public Matrix(List<List<double>> coefficients)
        {
            this.matrix = coefficients;
        }

        public int RawCount => this.matrix.Count;
        public int ColumnCount => this.matrix[0].Count;

        public Matrix AddRow(int rowIndex1, int rowIndex2)
        {
            if (rowIndex1 >= this.RawCount || rowIndex2 >= this.RawCount)
            {
                throw new Exception();
            }

            for (int i = 0; i < this.ColumnCount; i++)
            {
                this.matrix[rowIndex1][i] += this.matrix[rowIndex2][i];
            }

            return this;
        }
        public Matrix Concat(Matrix matrix)
        {
            if (this.RawCount != matrix.RawCount)
            {
                throw new Exception();
            }

            for (int i = 0; i < this.RawCount; i++)
            {
                this.matrix[i].AddRange(matrix.GetRow(i));
            }

            return this;
        }

        public double GetElement(int rowIndex, int columnIndex)
        {
            if (rowIndex >= this.RawCount || columnIndex >= this.ColumnCount)
            {
                throw new Exception();
            }

            return this.matrix[rowIndex][columnIndex];
        }
        public List<double> GetRow(int rowIndex)
        {
            if (rowIndex >= this.RawCount)
            {
                throw new Exception();
            }

            return this.matrix[rowIndex];
        }
        public List<double> GetColumn(int columnIndex)
        {
            if (columnIndex >= this.ColumnCount)
            {
                throw new Exception();
            }

            var column = new List<double>();
            foreach (var row in this.matrix)
            {
                column.Add(row[columnIndex]);
            }

            return column;
        }

        public Matrix Mirror()
        {
            for (int i = 0; i < RawCount / 2; i++)
            {
                this.SwapRows(i, RawCount - i - 1);
            }

            return this;
        }

        public double MNorm()
        {
            double max = 0;

            for (int i = 0; i < this.RawCount; i++)
            {
                double rowSum = this.GetRow(i).Sum(x => Math.Abs(x));
                max = max > rowSum ? max : rowSum;
            }

            return max;
        }
        public double LNorm()
        {
            double max = 0;

            for (int i = 0; i < this.ColumnCount; i++)
            {
                double columnSum = this.GetColumn(i).Sum(x => Math.Abs(x));
                max = max > columnSum ? max : columnSum;
            }

            return max;
        }

        public Matrix MultiplyRow(int rowIndex, double number)
        {
            if (rowIndex >= this.RawCount)
            {
                throw new Exception();
            }

            if (number == 0)
            {
                throw new Exception();
            }

            for (int i = 0; i < this.ColumnCount; i++)
            {
                this.matrix[rowIndex][i] *= number;
            }

            return this;
        }
        public Matrix SwapRows(int rowIndex1, int rowIndex2)
        {
            var temp = this.GetRow(rowIndex1);
            this.matrix[rowIndex1] = this.GetRow(rowIndex2);
            this.matrix[rowIndex2] = temp;
            return this;
        }

        public Matrix Add(Matrix matrix)
        {
            if (this.RawCount != matrix.RawCount || this.ColumnCount != matrix.ColumnCount)
            {
                throw new Exception();
            }

            for (int i = 0; i < this.RawCount; i++)
            {
                for (int j = 0; j < this.ColumnCount; j++)
                {
                    this.matrix[i][j] += matrix.GetElement(i, j);
                }
            }

            return this;
        }
        public Matrix Subtract(Matrix matrix)
        {
            if (this.RawCount != matrix.RawCount || this.ColumnCount != matrix.ColumnCount)
            {
                throw new Exception();
            }

            for (int i = 0; i < matrix.RawCount; i++)
            {
                matrix.MultiplyRow(i, -1);
            }

            return this.Add(matrix);
        }
    }
}